function getType(thing) {
	
	var type = typeof thing;
	if(type == "object") {
		if(thing) {
			return thing instanceof Array ? "array" : type;
		}
		return "null";
	}
	return type;
}

var STATE = "state";
var COLLAPSED = "collapsed";
var EXPANDED = "expanded";

// create a level, initial state: collapsed
function attach(json, name) {

	var el = document.getElementById(name);
	//console.log("attach: " + name + ": " + el.getAttribute("state") );
	
	if( el.getAttribute(STATE) == EXPANDED ) {
	//	console.log("state: " + el.getAttribute(STATE) + " " + name);
		return;
	}
	
//	console.log("state was not expanded, creating, cc: " + el.childNodes.length);
	if(el.childNodes.length > 1 ) { // if we made this before, then don't add another
		return;
	}
	
	if(json instanceof Object) {
		var is_array = json instanceof Array;
		var list = document.createElement("ul");
		var prefix = name.substr("dyntree".length + 1);
		list.setAttribute("class","dump");
		for(var key in json) {
			if (!json.hasOwnProperty(key))
				continue;
			var li = document.createElement("li");
			li.setAttribute("id", name + "." + key);
			li.setAttribute("class", getType(json[key]));
			if( json[key] instanceof Object ) {
				li.addEventListener('click', toggle.bind(json, name + "." + key), false); // bubble
				if (is_array)
					li.appendChild(document.createTextNode('#' + key));
				else
					li.appendChild(document.createTextNode(key));
				li.setAttribute(STATE, COLLAPSED);
				li.searchDescription = prefix ? prefix + "." + key : key;
				$(li).addClass('collapsible');
			} else {
				var key_node = document.createElement("b");
				key_node.appendChild(document.createTextNode(key));
				if (prefix) {
					var prefix_node = document.createElement("i");
					prefix_node.appendChild(document.createTextNode(prefix + ". "));
					li.appendChild(prefix_node);
				}
				li.appendChild(key_node);
				li.appendChild(document.createTextNode(": " + json[key]));
				li.addEventListener('click', end, true); // end bubble
				li.searchDescription = (prefix ? prefix + "." : "") + key + ": " + json[key];
			}
			list.appendChild(li);			
		}
	
		el.appendChild(list);
	} else {
	//	console.log("Adding non object, cc: " + el.childNodes.length);
		if(el.childNodes.length > 0) {
			return;
		}
		el.appendChild(document.createTextNode(" : " + json));		
	}
	

}

function end(event) {
	event.stopPropagation();
}

function toggle(qualifiedKey, event) {

	var li = document.getElementById(qualifiedKey);
	var key = qualifiedKey.split(".").pop();
	//console.log("qName: " + qualifiedKey + " -> " + key );
	var data = this[key];
	if( li.getAttribute(STATE) == COLLAPSED){
		attach(data, qualifiedKey);
		li.setAttribute(STATE, EXPANDED);
		$(li).addClass('expanded');
	} else {
		li.setAttribute(STATE, COLLAPSED);
		li.removeChild(li.childNodes[1]);
		$(li).removeClass('expanded');
	}

	event.stopPropagation(); // stop bubble up
}

/*
expand each path element (Since each potentially needs to be created)
*/
var currentHighlights = [];
function expand(path, data) {

	var qName = "dyntree";

	path.forEach(function(el, idx, arr){
		qName += "." + el;
		data = data[el];
		attach(data, qName);
		var el = document.getElementById(qName);
		el.setAttribute(STATE, EXPANDED);
		$(el).addClass('expanded');
		if(idx == arr.length-1) {
			$(el).addClass(getType(data) + " highlight");
			currentHighlights.push(el);
			currentSearchResults.push(el);
		}
	});	
	
}

function expandAll(data, prefix) {
	for (;;) {
		var collapsed = $('[' + STATE + '="' + COLLAPSED + '"]');
		if (collapsed.size() == 0)
			return;
		if (collapsed.size() == 1 && collapsed.get(0).id == prefix)
			return;
		collapsed.each(function (index, el) {
			if (el.id != prefix)
				el.click();
		});
	}
}

/**
 * Find all instances of a specific key and expand those (and hide all other paths)
 * go depth first to create path (collapsing everything on the way), exhuastively search the entire tree and then call toggle on all paths 
 */
var currentSearchResults = [];
var searchMessage = null;
var CSRIndex = 0;
function search(el, data) {

	CSRIndex = 0;
	currentSearchResults = [];
	searchMessage = null;
	// clear all current highlights
	currentHighlights.forEach(function(el, idx, arr){
		$(el).removeClass("highlight");
	});

//	console.log("looking for: " + el.value);
	if(el.value.length == 0) {
		$('#sr_pane').remove();
		SR_Pane = null;
		return;
	}
	if(el.value.length < 2  || (el.value.substr(0,2) == "b_" && el.value.length < 4)) {
		searchMessage = "Type some more characters in the search box";
		srpane();
		return;
	}
	try{
		var re = new RegExp(el.value);
	} catch(e) {
		// just swallow it, maybe you're typing in "foo(b" and arent done yet
		return;	
	}
	var paths = find( re, data);
//	console.log("found: ", paths);
	paths.forEach(function(el, idx, arr){
		expand(el, data);
	});

	if(currentSearchResults.length > 0) {
		scrollToElement(currentSearchResults[0]);
	} else {
		searchMessage = "No results found";
	}
	srpane();
}

function scrollToElement(el) {
	el.scrollIntoView(true);
}

function find(regex, data) {

	var out = [];

	for(var key in data) {
		if( regex.test(key) ) {
			out.push([key]); // path is a list of keys
		}
		if( typeof data[key] == "object" && data[key] ) {
			// console.log("recursing " + key);
			var subPaths = find(regex, data[key]);
			subPaths.forEach(function(el, idx, arr){
				out.push([key].concat(el)); 
		 	});
			
		}				
	}

	return out;
}

function collapse(qName){
	var li = document.getElementById(qName);
	li.setAttribute(STATE, COLLAPSED);
	li.removeChild(li.childNodes[0]);
	$(li).removeClass('expanded');
}

/**
 * Open a pane with only the search results
 * @returns
 */
var SR_Pane;
function srpane() {
	
	// all those things are HTML li elements, so just make a list

	// first time, create it
	if(SR_Pane == undefined) {
		SR_Pane = document.createElement("ol");
		SR_Pane.setAttribute("id", "sr_pane");
		document.getElementById("dynheader").appendChild(SR_Pane);
	}

	while(SR_Pane.hasChildNodes()) {
		SR_Pane.removeChild(SR_Pane.lastChild);
	}
	
	
	currentSearchResults.forEach( function(el, idx, arr){
		var clone = document.createElement("div");
		clone.setAttribute("linkto", el.getAttribute("id")); // avoid dupe IDs but remember it
		clone.addEventListener('click', scrollToElement.bind(this, el), false); // click moves main thing to item
		clone.textContent = el.searchDescription;
		SR_Pane.appendChild(clone);	
	} );

	if (currentSearchResults.length == 0 && searchMessage) {
		var clone = document.createElement("div");
		clone.textContent = searchMessage;
		SR_Pane.appendChild(clone);
	}
}

jQuery(function($j) {
	var loaded = false;
	var url = $j('#plDebugWindowBookingContentUrl').val();
	$j.plDebugBooking = function(data, klass) {
		$j.plDebugBooking.init();
	}
	$j.extend($j.plDebugBooking, {
		init: function() {
			if (!url)
				return;

			$j('#plDebugPanelList li a').click(function() {
				if ($j(this).attr('href') == '#') {
					if (!this.className)
						return false;
					if (!loaded)
						$j.plDebugBooking.loadContent();
					return false;
				}

				document.location = $j(this).attr('href');
			});
		},
		loadContent: function () {
			$j.ajax({
				url: url,
				dataType: 'json',
				success: function (content) {
					loaded = true;
					for (var id in content)
						$j('#plDebug #' + id + ' .scroll').html(content[id]);
				},
				error: function () {
					alert("Error while loading panel content");
				}
			});
		}
	});
	
	var roles = ['intranet' ,'bookadmin', 'controlrooms'];
	if(roles.indexOf($j('#persona').val()) != -1) {
	    var nav_title = $j("#plDebugWindowBookingContentUrl").val();
	    var cur_warnings_bar = $j("#plDebugPanelList").find("li").find("a[title='Current Warnings']");
        var prod_warnings_bar = $j("#plDebugPanelList").find("li").find("a[title='Production Status']");
	    if( nav_title != '') {
            var wait_for_panel = window.setTimeout(function() {
                $j.ajax({
                    url: nav_title,
                    dataType: 'json',
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        var warnings = getValByRegex(data, 'plDebugCurrentWarnings'+"\\d*?" + 'Panel');
                        var warnings_count = 0;
                        if(warnings != null) {
                            warnings_count = $(warnings).find("td").filter(":contains('count')").
                                     next().find("pre").html();
                        }
                        if(warnings_count > 0) {
                            cur_warnings_bar.css('background-color', 'red');
                            drawBadge(cur_warnings_bar, { value: warnings_count, style:'' });
                            $j("#plShowToolBarButton").click();
                        }

                        var warn_panel = getValByRegex(data, 'plDebugProductionStatus'+"\\d*?" + 'Panel');
                        if(warn_panel != null) {
                            var the_action = $(warn_panel).filter("[name='this_action']").val();
                            var targets = [];
                            var base = 'smartSummarize(monitors.be_perf.by_persona.' + $j("#persona").val()
                                       + '.' + the_action;
                            targets.push(base + ".nr_warnings_sum, '60min', 'sum')");
                            targets.push(base + ".nr_errors_sum, '60min', 'sum')");
                            if(the_action) {
                                var graphite_url = 'https://graphite.booking.com/render/';
                                $j.ajax({
                                    url: graphite_url,
                                    data: { 
                                        format: 'json',
                                        target: targets,
                                        from: '-4hours'
                                        },
                                    dataType: 'json',
                                    async: false,
                                    success: function(data, status, xhr) {

                                        if (typeof data === 'undefined' || data.length === 0)
                                            return;

                                        var prod_warns_count = 0;
                                        var badge_data = {
                                                            warnings: {
                                                                value: 0,
                                                                style: "style='color: black;background-color: darkorange'"
                                                            },
                                                            errors: {
                                                                value: 0,
                                                                style: "style='background-color: red'"
                                                            }
                                                        };
                                        $.each(data, function(i) {
                                            var path = (data[i].target.split(','))[0].split('.');
                                            var key = path.pop();
                                                $.each(data[i].datapoints, function(index, val) {
                                                    // format of data.datapoints
                                                    // [[0, 1405501201], [0, 1405504801], [0, 1405508401]...]
                                                    if(key.match(/warnings/)) {
                                                        badge_data.warnings.value += val[0];
                                                    }
                                                    else if(key.match(/errors/)) {
                                                        badge_data.errors.value += val[0];
                                                    }
                                                });
                                        });
                                        if(badge_data.errors.value) {
                                            badge_data.errors.value += badge_data.errors.value > 1 ?
                                                ' errors' : ' error';
                                            drawBadge(prod_warnings_bar, badge_data.errors);
                                        }
                                        if(badge_data.warnings.value) {
                                            badge_data.warnings.value += badge_data.warnings.value > 1 ?
                                                ' warnings' : ' warning';
                                            drawBadge(prod_warnings_bar, badge_data.warnings);
                                        }
                                        $j("#plShowToolBarButton").click();
                                    }
                                });
                            }
                        }
                    }
                });
            }, 1000);
	    }
	}

});
jQuery(function() {
	jQuery.plDebugBooking();
});

function getValByRegex(jsonObj, pattern) {
    var re = new RegExp(pattern), key;
    for(key in jsonObj) {
        if(re.test(key)) {
            return jsonObj[key];
        }
    }
    return null;
}

function drawBadge(element, badge_attrs) {
    element.html(element.html() + ' <span class="badge" ' +
        (badge_attrs.style != '' ? badge_attrs.style : '') + '>' + badge_attrs.value + '</span>');
}
