jasmine.getFixtures().fixturesPath = '/Users/mbenedetto/Sites/test/test';

describe("First test", function() {
   
  it ("should load index and say ok", function() {
    loadFixtures('index.html');
    expect($('div')).toExist();
  });
});
  
// describe("Hide message tests", function() {
//   beforeEach(function() {
//     setUpHTMLFixture();
//     $('#pMsg').text(MSG);
//     $('#btnHideMessage').trigger( "click" );
//   });
   
//   it ("should remove the message when button is clicked.", function() {
//     expect($('#pMsg')).toHaveText("");
//   });
// });